import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { DynamicListModule } from '../../factories/list/dynamic-list.module';
//import { DynamicFormModule } from '../../factories/form/dynamic-form.module';
import { APIService } from '../../providers/api.service';
import { RouterModule } from '@angular/router';

import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  imports: [
    NgMultiSelectDropDownModule.forRoot(),
    BrowserAnimationsModule,
    CommonModule,
    NgxSpinnerModule,
    //DynamicListModule,
    //DynamicFormModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
  ],
  declarations: [
    UserProfileComponent
  ],
  entryComponents: [
    UserProfileComponent
  ],
  providers: [
    APIService,
  ],
})
export class ProfileModule { }
