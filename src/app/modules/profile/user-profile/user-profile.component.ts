import { Component, OnInit, Input,ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { APIService } from '../../../providers/api.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  profileForm: FormGroup;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  constructor(private spinner: NgxSpinnerService,private __apiService: APIService, private __fb: FormBuilder,private cd: ChangeDetectorRef) {
      this.profileForm = __fb.group({
          "first_name": ["", Validators.compose([Validators.required])],
          "last_name": ["", Validators.compose([Validators.required])],
          "email": ["", Validators.compose([Validators.required])],
          "categories": ["", Validators.compose([Validators.required])],

      });
  }


  update() {
    if (this.profileForm.valid) {
      this.spinner.show();
      this.__apiService.post("user/update", this.profileForm.value).then(result => {
          alert(JSON.stringify(result));
          this.spinner.hide();
      }).catch(err => {
          alert(JSON.stringify(err.error));
          this.spinner.hide();
      });
    }
  }

  getProfile(){
    this.spinner.show();
    this.__apiService.post('auth/me',{}).then(profile=>{
      this.profileForm.patchValue(profile);
      this.spinner.hide();
    });
  }

  get_categories(){
    this.spinner.show();
    this.__apiService.get('category').then((categories:any)=>{
      this.dropdownList = categories;
      this.spinner.hide();
    });
  }
  ngOnInit() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.getProfile();
    this.get_categories();
  }

}
