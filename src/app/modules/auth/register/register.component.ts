import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailValidator } from '../../../validators/email_validator';
import { APIService } from '../../../providers/api.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  constructor(private spinner: NgxSpinnerService,private __apiService: APIService, private __fb: FormBuilder) {
      this.registerForm = __fb.group({
          "first_name": ["", Validators.compose([Validators.required])],
          "last_name": ["", Validators.compose([Validators.required])],
          "email": ["", Validators.compose([Validators.required, EmailValidator.isValid])],
          "password": ["", Validators.compose([Validators.required])]
      });
  }

  register() {
      if (this.registerForm.valid) {
          this.spinner.show();
          let info = this.registerForm.value;
          let data = {
              "email": info.email,
              "password": info.password,
              "first_name": info.first_name,
              "last_name": info.last_name,
          }
          this.__apiService.post("auth/register", data).then(result => {
            this.spinner.hide();
            alert("Success , please login");
          }).catch(err => {
            this.spinner.hide();
            alert(JSON.stringify(err.error));
          });
      }
  }
  ngOnInit() {

  }

}
