import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { APIService } from '../../../../providers/api.service';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

    ResetPasswordForm: FormGroup;
    constructor(private apiService: APIService, fb: FormBuilder)
    {
        this.ResetPasswordForm  = fb.group({
            "password": ["", Validators.compose([Validators.required, Validators.minLength(8)])],
            "passwordConfirm": ["", Validators.compose([Validators.required, Validators.minLength(8)])],

        });
    }
    resetPassword()
    {
        if (this.ResetPasswordForm.valid)
        {
            let info = this.ResetPasswordForm.value;
            let password = info.password;
            let passConfirm = info.passConfirm;
            this.apiService.post("reset", { email: passConfirm }).then(result =>
            {
                console.log(result);

            }).catch(err =>
            {
                console.log(err);

            });

        }
    }
  ngOnInit() {
  }

}
