import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { APIService } from '../../../providers/api.service';
import { EmailValidator } from '../../../validators/email_validator';
import { Router } from '@angular/router';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
    forgotPasswordForm: FormGroup;
    constructor(private apiService: APIService, private fb: FormBuilder, private router: Router) {
        this.forgotPasswordForm = this.fb.group({
            "email": ["", Validators.compose([Validators.required, EmailValidator.isValid])],
        });
    }
    submit(captchaResponse: string) {
        if (this.forgotPasswordForm.valid) {
            let info = this.forgotPasswordForm.value;
            let email = info.email;
            this.apiService.post("reset", { email: email }).then(result => {
                console.log(result);

                this.router.navigate(['/forgotPassword/emailSent']);


            }).catch(err => {
                console.log(err);

            });

        }
    }
    ngOnInit() {
    }

}
