import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-password-email-sent',
  templateUrl: './reset-password-email-sent.component.html',
  styleUrls: ['./reset-password-email-sent.component.css']
})
export class ResetPasswordEmailSentComponent implements OnInit {

  constructor(private router : Router) { }
    redirectToReset()
    {
        this.router.navigate(['/forgotPassword']);

    }
  ngOnInit() {
  }

}
