import { NgModule } from '@angular/core';
import { RecaptchaModule } from 'ng-recaptcha';
import { BrowserModule } from '@angular/platform-browser';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { RECAPTCHA_SETTINGS, RecaptchaSettings } from 'ng-recaptcha';

import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { DynamicListModule } from '../../factories/list/dynamic-list.module';
//import { DynamicFormModule } from '../../factories/form/dynamic-form.module';
import { APIService } from '../../providers/api.service';
import { RouterModule } from '@angular/router';
import { DoneComponent } from './forgot-password/done/done.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './forgot-password/reset-password/reset-password.component';
import { ResetPasswordEmailSentComponent } from './forgot-password/reset-password-email-sent/reset-password-email-sent.component';

import { NgxSpinnerModule } from 'ngx-spinner';

import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angular5-social-login";
// Configs

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider("1975846669328279")
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("1090096002566-i51d9vq4mb5ftsko452nfs84epgifbdr.apps.googleusercontent.com")
      },
    ]
  );
  return config;
}

@NgModule({
  imports: [
    CommonModule,
    //DynamicListModule,
    //DynamicFormModule,
    RouterModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    RecaptchaFormsModule,
    SocialLoginModule,
    RecaptchaModule.forRoot()
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    DoneComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    ResetPasswordEmailSentComponent
  ],
  entryComponents: [
    RegisterComponent
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    APIService,
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: { siteKey: '6LcfUioUAAAAANknIlbaZ00xZpmQuKhprlg64HaT' } as RecaptchaSettings,
    },
  ],
})
export class AuthModule { }
