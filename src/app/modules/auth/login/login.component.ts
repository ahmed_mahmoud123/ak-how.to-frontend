import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailValidator } from '../../../validators/email_validator';
import { APIService } from '../../../providers/api.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

/*import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular5-social-login';*/

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup;
  constructor(/*private socialAuthService: AuthService,*/private spinner: NgxSpinnerService,private router:Router,private __apiService: APIService, private __fb: FormBuilder) {
    this.loginForm = __fb.group({
      "email": ["", Validators.compose([Validators.required, EmailValidator.isValid])],
      "password": ["", Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
  }


  login() {
    if (this.loginForm.valid) {
      this.spinner.show();
      let info = this.loginForm.value;
      this.__apiService.post("auth/login", info).then((result:any) => {
        this.spinner.hide();
        localStorage.setItem("api_key",result.token);
        this.router.navigate(['/dashboard']);
      }).catch(err => {
        this.spinner.hide();
        alert(JSON.stringify(err));
      });
    }
  }

  /*public socialSignIn(evt, socialPlatform: string) {
    evt.preventDefault();
    let socialPlatformProvider;
    if (socialPlatform == "facebook") {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        alert(JSON.stringify(userData));
      }
    ).catch(err => {
      console.log(err);
    });
  }*/


}
