import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { CommonModule } from '@angular/common';
import { CreateSchoolComponent } from './create/create.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { DynamicListModule } from '../../factories/list/dynamic-list.module';
//import { DynamicFormModule } from '../../factories/form/dynamic-form.module';
import { APIService } from '../../providers/api.service';
import { RouterModule } from '@angular/router';
import { ListSchoolsComponent } from './list/list.component';
import { ViewSchoolComponent } from './view-school/view-school.component';
import { CreateCourseComponent } from './create-course/create-course.component';

import { ListCoursesComponent } from './list-courses/list-courses.component';

import { BuildCourseComponent } from './build-course/build-course.component';

import { ChannelModule } from '../channel/channel.module';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  imports: [
    NgxSpinnerModule,
    ChannelModule,
    NgMultiSelectDropDownModule.forRoot(),
    CommonModule,
    //DynamicListModule,
    //DynamicFormModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
  ],
  declarations: [
    CreateSchoolComponent,
    ListSchoolsComponent,
    ViewSchoolComponent,
    CreateCourseComponent,
    ListCoursesComponent,
    BuildCourseComponent
  ],
  entryComponents: [
    CreateSchoolComponent,
    ListSchoolsComponent,
    ViewSchoolComponent,
    CreateCourseComponent,
    ListCoursesComponent,
    BuildCourseComponent
  ],
  providers: [
    APIService,
  ],
})
export class SchoolModule { }
