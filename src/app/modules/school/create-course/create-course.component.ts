import { Component, OnInit, Input,ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { APIService } from '../../../providers/api.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-create-course',
    templateUrl: './create-course.component.html',
    styleUrls: ['./create-course.component.css']
})
export class CreateCourseComponent implements OnInit {

  createCourseForm: FormGroup;

  constructor(private spinner: NgxSpinnerService,private __apiService: APIService,private __activatedRoute:ActivatedRoute, private __fb: FormBuilder,private cd: ChangeDetectorRef) {
      this.createCourseForm = __fb.group({
          "name": ["", Validators.compose([Validators.required])],
          "desc": ["", Validators.compose([Validators.required])],
          "school_id": ["", Validators.compose([Validators.required])]
      });
  }


  create() {
      if (this.createCourseForm.valid) {
        this.spinner.show();
        this.__apiService.postForm("course/create", this.createCourseForm.value).then(result => {
            alert(JSON.stringify(result));
            this.spinner.hide();
        }).catch(err => {
            alert(JSON.stringify(err.error));
            this.spinner.hide();
        });
      }
  }

  ngOnInit() {
    let school_id = this.__activatedRoute.snapshot.parent.params['school_id'];
    this.createCourseForm.patchValue({school_id});
    this.cd.markForCheck();
  }

}
