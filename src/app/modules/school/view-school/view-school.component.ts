import { Component, OnInit, Input } from '@angular/core';
import { APIService } from '../../../providers/api.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-school-view',
    templateUrl: './view-school.component.html',
    styleUrls: ['./view-school.component.css']
})
export class ViewSchoolComponent implements OnInit {

  school:any = {};
  school_id:string;
  constructor(private spinner: NgxSpinnerService,private __apiService: APIService,private __activatedRoute:ActivatedRoute) {

  }



  ngOnInit() {
    this.school_id = this.__activatedRoute.snapshot.params['school_id'];
    this.spinner.show();
    this.__apiService.get(`school/${this.school_id}/details`).then((school:any)=>{
      this.school = school;
      this.spinner.hide();
    });
  }

}
