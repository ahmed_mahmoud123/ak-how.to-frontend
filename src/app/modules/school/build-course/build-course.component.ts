import { Component, OnInit, Input,ChangeDetectorRef } from '@angular/core';
import { APIService } from '../../../providers/api.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-build-course',
    templateUrl: './build-course.component.html',
    styleUrls: ['./build-course.component.css']
})
export class BuildCourseComponent implements OnInit {

  course:any = {};
  createLevelForm: FormGroup;
  levels:Array<any> = [];
  course_id:any;
  current_level:any;
  level_videos:Array<any> = [];
  constructor(private spinner: NgxSpinnerService,private __apiService: APIService,private __activatedRoute:ActivatedRoute,private __fb: FormBuilder,private cd: ChangeDetectorRef) {
    this.createLevelForm = __fb.group({
        "name": ["", Validators.compose([Validators.required])],
        "course_id": ["", Validators.compose([Validators.required])],
        "duration": ["", Validators.compose([Validators.required])]
    });
  }



  ngOnInit() {
    this.course_id = this.__activatedRoute.snapshot.params["course_id"];
    this.createLevelForm.patchValue({"course_id":this.course_id});
    this.cd.markForCheck();
    this.spinner.show();
    this.__apiService.get("course/"+this.course_id+"/details").then((course:any)=>{
      this.course = course;
      this.spinner.hide();
    });
    this.get_levels();
  }


  createLevel(){
    this.spinner.show();
    this.__apiService.post('level/create',this.createLevelForm.value).then((level:any)=>{
      this.current_level = level.id;
      this.spinner.hide();
      this.get_levels();
    });
  }

  get_levels(){
    this.spinner.show();
    this.__apiService.get('level/'+this.course_id).then((levels:any)=>{
      this.spinner.hide();
      this.levels = levels;
      if(this.levels[0]){
        this.current_level = this.levels[0].id;
        this.get_level_videos(this.current_level);
      }
    });
  }

  get_level_videos(level_id){
    this.__apiService.get('level/'+level_id+"/videos").then((videos:any)=>{
      this.level_videos = videos;
    });
  }

  get_level_courses(level_id){
    this.current_level = level_id;
    this.get_level_videos(level_id);
  }

  afterVideoPublished(){
    this.get_level_videos(this.current_level);
  }

}
