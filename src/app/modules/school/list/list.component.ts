import { Component, OnInit, Input } from '@angular/core';
import { APIService } from '../../../providers/api.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-channel-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListSchoolsComponent implements OnInit {

  schools:Array<any> = [];
  constructor(private spinner: NgxSpinnerService,private __apiService: APIService) {

  }



  ngOnInit() {
    this.spinner.show();
    this.__apiService.get("school/list").then((schools:any)=>{
      this.schools = schools;
      this.spinner.hide();
    });
  }

}
