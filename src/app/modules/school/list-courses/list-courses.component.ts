import { Component, OnInit, Input } from '@angular/core';
import { APIService } from '../../../providers/api.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-list-courses',
    templateUrl: './list-courses.component.html',
    styleUrls: ['./list-courses.component.css']
})
export class ListCoursesComponent implements OnInit {

  courses:Array<any> = [];
  parent:boolean = false;
  constructor(private spinner: NgxSpinnerService,private __apiService: APIService,private __activatedRoute:ActivatedRoute) {

  }



  ngOnInit() {

    let school_id = this.__activatedRoute.snapshot.parent.params['school_id'];
    if(school_id)
      this.parent = true;
    else{
      school_id = this.__activatedRoute.snapshot.params['school_id'];
      this.parent = false;
    }
    this.spinner.show();
    this.__apiService.get(`school/${school_id}/courses`).then((courses:any)=>{
      this.courses = courses;
      this.spinner.hide();
    });
  }


}
