import { Component, OnInit, Input } from '@angular/core';
import { APIService } from '../../../providers/api.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-channel-view',
    templateUrl: './view-channel.component.html',
    styleUrls: ['./view-channel.component.css']
})
export class ViewChannelsComponent implements OnInit {

  channel:any = {};
  channel_id:string;
  constructor(private spinner: NgxSpinnerService,private __apiService: APIService,private __activatedRoute:ActivatedRoute) {

  }



  ngOnInit() {
    this.channel_id = this.__activatedRoute.snapshot.params['channel_id'];
    this.spinner.show();
    this.__apiService.get(`channel/${this.channel_id}/details`).then((channel:any)=>{
      this.channel = channel;
      this.spinner.hide();
    });
  }

}
