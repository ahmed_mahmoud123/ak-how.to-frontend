import { Component, OnInit, Input,ChangeDetectorRef,Output,EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { APIService } from '../../../providers/api.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-publish-video',
    templateUrl: './publish-video.component.html',
    styleUrls: ['./publish-video.component.css']
})
export class ChannelPublishVideoComponent implements OnInit {

  channel:any = {};
  publishVideoForm: FormGroup;
  @Input('levelRelation') levelRelation:boolean = false;
  @Input('level_id') level_id:string;
  @Output('afterVideoPublished') afterVideoPublished:EventEmitter<any> = new EventEmitter();

  constructor(private spinner: NgxSpinnerService,private __apiService: APIService,private __activatedRoute:ActivatedRoute, private __fb: FormBuilder,private cd: ChangeDetectorRef) {
    this.publishVideoForm = __fb.group({
        "name": ["", Validators.compose([Validators.required])],
        "link": ["", Validators.compose([Validators.required])],
        "desc": ["", Validators.compose([Validators.required])],
        "duration": ["", Validators.compose([Validators.required])],
        "cover": ["", Validators.compose([Validators.required])],
        "relation_type":["channel"],
        "relation_id":[""]
    });
  }


  fileUpload(element){
    if(element.files && element.files[0]){
      let propertyName = element.id
      let obj = {};
      obj[propertyName] = element.files[0];
      this.publishVideoForm.patchValue(obj);
      this.cd.markForCheck();
    }
  }

  ngOnInit() {
    if(!this.levelRelation){
      let channel_id = this.__activatedRoute.snapshot.parent.params['channel_id'];
      this.publishVideoForm.patchValue({"relation_id":channel_id});
      this.cd.markForCheck();
    }
  }

  publish() {
    if (this.publishVideoForm.valid) {
      this.spinner.show();
      if(this.levelRelation){
        this.publishVideoForm.patchValue({"relation_id":this.level_id});
        this.publishVideoForm.patchValue({"relation_type":"level"});
        this.cd.markForCheck();
      }
      this.__apiService.postForm("video/publish", this.publishVideoForm.value).then(result => {
          alert(JSON.stringify(result));
          this.afterVideoPublished.emit(result);
          this.spinner.hide();
      }).catch(err => {
          alert(JSON.stringify(err.error));
          this.spinner.hide();
      });
    }
  }

}
