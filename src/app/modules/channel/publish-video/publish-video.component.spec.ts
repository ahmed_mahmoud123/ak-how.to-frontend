import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelPublishVideoComponent } from './publish-video.component';

describe('ChannelPublishVideoComponent', () => {
  let component: ChannelPublishVideoComponent;
  let fixture: ComponentFixture<ChannelPublishVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannelPublishVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelPublishVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
