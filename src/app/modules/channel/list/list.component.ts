import { Component, OnInit, Input } from '@angular/core';
import { APIService } from '../../../providers/api.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-channel-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListChannelsComponent implements OnInit {

  channels:Array<any> = [];
  constructor(private spinner: NgxSpinnerService,private __apiService: APIService) {

  }



  ngOnInit() {
    this.spinner.show();
    this.__apiService.get("channel/list").then((channels:any)=>{
      this.channels = channels;
      this.spinner.hide();
    });
  }

}
