import { Component, OnInit, Input,ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { APIService } from '../../../providers/api.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-channel-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css']
})
export class CreateChannelComponent implements OnInit {

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  createChannelForm: FormGroup;
  constructor(private spinner: NgxSpinnerService,private __apiService: APIService, private __fb: FormBuilder,private cd: ChangeDetectorRef) {
      this.createChannelForm = __fb.group({
          "name": ["", Validators.compose([Validators.required])],
          "link": ["", Validators.compose([Validators.required])],
          "desc": ["", Validators.compose([Validators.required])],
          "avatar": ["", Validators.compose([Validators.required])],
          "cover": ["", Validators.compose([Validators.required])],
          "categories": ["", Validators.compose([Validators.required])]
      });
  }

  fileUpload(element){
    if(element.files && element.files[0]){
      let propertyName = element.id
      let obj = {};
      obj[propertyName] = element.files[0];
      this.createChannelForm.patchValue(obj);
      this.cd.markForCheck();
    }
  }
  create() {
      if (this.createChannelForm.valid) {
        this.spinner.show();
        let obj = this.createChannelForm.value;
        obj.categories = JSON.stringify(obj.categories);
        this.__apiService.postForm("channel/create", obj).then(result => {
          this.spinner.hide();
          alert(JSON.stringify(result));
        }).catch(err => {
          alert(JSON.stringify(err.error));
        });
      }
  }

  get_categories(){
    this.spinner.show();
    this.__apiService.get('category').then((categories:any)=>{
      this.spinner.hide();
      this.dropdownList = categories;
    });
  }

  ngOnInit() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.get_categories();
  }

}
