import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { CommonModule } from '@angular/common';
import { CreateChannelComponent } from './create/create.component';
import { ListChannelsComponent } from './list/list.component';

import { ViewChannelsComponent } from './view-channel/view-channel.component';
import { ChannelPublishVideoComponent } from './publish-video/publish-video.component';
import { ChannelListVideosComponent } from './list-videos/list-videos.component';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { DynamicListModule } from '../../factories/list/dynamic-list.module';
//import { DynamicFormModule } from '../../factories/form/dynamic-form.module';
import { APIService } from '../../providers/api.service';
import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  imports: [
    NgMultiSelectDropDownModule.forRoot(),
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    NgxSpinnerModule
  ],
  declarations: [
    CreateChannelComponent,
    ListChannelsComponent,
    ViewChannelsComponent,
    ChannelPublishVideoComponent,
    ChannelListVideosComponent
  ],
  entryComponents: [
    CreateChannelComponent,
    ListChannelsComponent,
    ViewChannelsComponent,
    ChannelPublishVideoComponent,
    ChannelListVideosComponent
  ],
  providers: [
    APIService,
  ],
  exports:[
    ChannelPublishVideoComponent
  ]
})
export class ChannelModule { }
