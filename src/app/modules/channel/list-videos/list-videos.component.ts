import { Component, OnInit, Input } from '@angular/core';
import { APIService } from '../../../providers/api.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-channel-list-videos',
    templateUrl: './list-videos.component.html',
    styleUrls: ['./list-videos.component.css']
})
export class ChannelListVideosComponent implements OnInit {

  vidoes:Array<any> = [];
  parent:boolean = false;
  constructor(private spinner: NgxSpinnerService,private __apiService: APIService,private __activatedRoute:ActivatedRoute) {

  }



  ngOnInit() {
    this.spinner.show();
    let channel_id = this.__activatedRoute.snapshot.parent.params['channel_id'];
    if(channel_id)
      this.parent = true;
    else{
      channel_id = this.__activatedRoute.snapshot.params['channel_id'];
      this.parent = false;
    }
    this.__apiService.get(`channel/${channel_id}/videos`).then((videos:any)=>{
      this.vidoes = videos;
      this.spinner.hide();
    });
  }


}
