import { Component, OnInit } from '@angular/core';
import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from 'ngx-paypal';


@Component({
  selector: 'app-paypal',
  templateUrl: './paypal.component.html',
  styleUrls: ['./paypal.component.css']
})
export class PaypalComponent implements OnInit {

  public payPalConfig?: PayPalConfig;
  constructor() {}

  ngOnInit(): void {
    this.initConfig();
  }

  private initConfig(): void {
     this.payPalConfig = new PayPalConfig(PayPalIntegrationType.ClientSideREST, PayPalEnvironment.Sandbox, {
       commit: true,
       client: {
         sandbox: 'AbZqCIP9IZG2U_Yi7KpWtGhtP2L9Ey0H0i36_TzbpX_Qs1Shs1y_XPcRDCZSNatvmRM0SPMYKtGwAH1a'
       },
       button: {
         label: 'paypal',
       },
       onPaymentComplete: (data, actions) => {
         alert('OnPaymentComplete');
       },
       onCancel: (data, actions) => {
         console.log('OnCancel');
       },
       onError: (err) => {
         console.log('OnError');
       },
       transactions: [{
         amount: {
           currency: 'USD',
           total: 9
         }
       }]
     });
   }









}
