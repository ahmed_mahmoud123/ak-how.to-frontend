import { Component, OnInit } from '@angular/core';
import { APIService } from '../../providers/api.service';
import {Router} from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers:[APIService]
})
export class HomeComponent implements OnInit {


  videos:Array<any>= []
  constructor(private spinner: NgxSpinnerService,private __router:Router,private __genericService: APIService) {
  }

  ngOnInit() {
    this.spinner.show();
    this.__genericService.get("video/list").then((videos:any)=>{
      this.videos = videos;
      this.spinner.hide();
    });
  }

  viewVideo(video){
      this.__router.navigate(['/dashboard/video',video.id])
  }




}
