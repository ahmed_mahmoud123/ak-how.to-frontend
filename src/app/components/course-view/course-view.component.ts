import { Component, OnInit } from '@angular/core';
import { APIService } from '../../providers/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-course-view',
  templateUrl: './course-view.component.html',
  styleUrls: ['./course-view.component.css'],
  providers:[APIService]
})
export class CourseViewComponent implements OnInit {

  course:any = {};
  levels:Array<any> = [];
  course_id:any;
  level_videos:any = {};
  current_video:any;
  constructor(private __activatedRoute:ActivatedRoute,private __apiService: APIService) {

  }

  ngOnInit() {
    this.course_id = this.__activatedRoute.snapshot.params["course_id"];
    this.__apiService.get("course/"+this.course_id+"/details").then((course:any)=>{
      this.course = course;
    });
    this.get_levels();
  }

  get_levels(){
    this.__apiService.get('level/'+this.course_id).then((levels:any)=>{
      this.levels = levels.reverse();
      for (let i = 0; i < this.levels.length; i++) {
          this.get_level_videos(this.levels[i].id);
      }
    });
  }

  get_level_videos(level_id){
    this.__apiService.get('level/'+level_id+"/videos").then((videos:any)=>{
      this.level_videos[level_id] = videos.reverse();
    });
  }

  play(evt,video){
    evt.preventDefault();
    this.current_video = video;
  }




}
