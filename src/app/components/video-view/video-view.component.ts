import { Component, OnInit } from '@angular/core';
import { APIService } from '../../providers/api.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-video-view',
  templateUrl: './video-view.component.html',
  styleUrls: ['./video-view.component.css'],
  providers:[APIService]
})
export class VideoViewComponent implements OnInit {

  answers:Array<any> = [];
  answerForm:any;
  videoDetails:any;
  videoChannelStatus:any = {liked:false,disliked:false,subscribe:false}
  constructor(private spinner: NgxSpinnerService,private __fb: FormBuilder,private __activatedRoute:ActivatedRoute,private __genericService: APIService) {
    this.answerForm = __fb.group({
        "answer": ["", Validators.compose([Validators.required])],
        "video_id": ["", Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    let video_id = this.__activatedRoute.snapshot.params['video_id'];
    this.answerForm.patchValue({video_id});
    this.spinner.show();
    this.__genericService.get('user/videoChannelStatus/'+video_id).then((videoChannelStatus:any)=>{
      this.videoChannelStatus = videoChannelStatus;
      this.spinner.hide();
    });
    this.spinner.show();
    this.__genericService.get('video/'+video_id).then((videoDetails:any)=>{
      this.videoDetails = videoDetails;
      this.spinner.hide();
      this.get_answers(this.videoDetails.video.id);
    });
  }

  liked(video_id){
    this.__genericService.get('video/'+video_id+'/like').then((result:any)=>{
      this.videoChannelStatus.liked = true;
      this.videoChannelStatus.disliked = false;
    });
  }

  disliked(video_id){
    this.__genericService.get('video/'+video_id+'/dislike').then((result:any)=>{
      this.videoChannelStatus.liked = false;
      this.videoChannelStatus.disliked = true;
    });
  }

  subscribe(channel_id){
    //this.spinner.show();
    let op = this.videoChannelStatus.subscribe?'unsubscribe':'subscribe';
    this.__genericService.get('channel/'+channel_id+'/'+op).then((result:any)=>{
      this.videoChannelStatus.subscribe = !this.videoChannelStatus.subscribe;
      //this.spinner.hide();
    });
  }

  answer(){
    if(this.answerForm.valid){
      this.spinner.show();
      this.__genericService.post('answer/create',this.answerForm.value).then((result:any)=>{
        this.get_answers(this.videoDetails.video.id);
        this.spinner.hide();
      });
    }
  }

  get_answers(video_id){
    this.__genericService.get('answer/'+video_id).then((answers:any)=>{
      this.answers = answers.map(answer=>{
        answer.answer.createdAt = new Date(answer.answer.createdAt);
        return answer;
      });
    });
  }




}
