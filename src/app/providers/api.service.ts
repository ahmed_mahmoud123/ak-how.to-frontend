import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable()
export class APIService {
  api_url: string;
  constructor(public http: HttpClient) {
    this.api_url = "https://api.how.to/";
    //this.api_url = "http://localhost:3000/";
  }


  setAuthorization(){
    let token = localStorage.getItem('api_key');
    let headers = undefined;
    if(!token)
      headers = new HttpHeaders({'Content-Type':'application/json'});
    else
      headers = new HttpHeaders({'Content-Type':'application/json',"Authorization": "Token " + token});
    return {headers:headers};
  }

  get(urn) {
    return this.http.get(`${this.api_url}${urn}`,this.setAuthorization()).toPromise();
  }

  post(urn, obj) {
    let headers = this.setAuthorization();
    return this.http.post(`${this.api_url}${urn}`, JSON.stringify(obj), headers).toPromise();
  }


  postForm(urn, obj) {
    let token = localStorage.getItem('api_key');
    let input = new FormData();
    for (let property in obj) {
        input.append(property,obj[property]);
    }
    let headers = new HttpHeaders({"Authorization": "Token " + token});
    return this.http.post(`${this.api_url}${urn}`,input, {headers}).toPromise();
  }




}
