import { FormControl } from '@angular/forms';


export class EmailValidator {

    constructor() { }

    static isValid(control: FormControl): any {
        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        if (!EMAIL_REGEXP.test(control.value)) {
            return { "email": true };
        }
        return null;
    }


}
