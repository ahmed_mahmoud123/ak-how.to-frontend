import { Component,Input,Output,OnInit,EventEmitter} from '@angular/core';
import { APIService } from '../../providers/api.service';


@Component({
  selector : "dynamic-list",
  templateUrl:"./dynamic-list.component.html",
  styleUrls:["./dynamic-list.component.css"]
})

/* Dynamic List Component to generate List depened on 'config' variable which has structure like {dataSource:'',colsNames:[],feildsName:[]} */
/* for 'config' variable structure kindly check models folder */
export class DynamicListComponent implements OnInit {

  data:Array<any>=[];
  total:Array<number>=[];
  page=1;
  working:boolean = true;
  query:any;
  @Output() rowClicked: any = new EventEmitter(); /*to fire event when row clicked and get row data*/
  @Input() config: any;
  @Input() set queryObject(value: any) {
    this.query = value;
    this.getData(1);
  }

  constructor(private apiService:APIService){

  }

  ngOnInit(){
    this.getData();
  }

  // get data using dataSource to populate list
  getData(page=1){
    this.working = true;
    this.apiService.post(`${this.config.dataSource}`,{
      page:page,
      query:this.query
    }).then((result:any)=>{
      this.working = false;
      this.data = result.rows;
      this.page = result.page;
      this.total = Array(result.pages);
    })
  }

  next(){
    if(this.page == this.total.length ) return;
    this.getData(++this.page);
  }

  prev(){
    if(this.page == 1 ) return;
    this.getData(--this.page);
  }


  clicked(e,row_id){
    e.preventDefault();
    this.rowClicked.emit(row_id);
  }




}
