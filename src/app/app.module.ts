import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RoutesHelper } from './routes/routes';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AuthModule } from './modules/auth/auth.module';
import { ChannelModule } from './modules/channel/channel.module';
import { SchoolModule } from './modules/school/school.module';
import { ProfileModule } from './modules/profile/profile.module';

import { HomeComponent } from './components/home/home.component';
import { PaypalComponent } from './components/paypal/paypal.component';
import { VideoViewComponent } from './components/video-view/video-view.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { NgxPayPalModule } from 'ngx-paypal';
import { LayoutComponent } from './components/layout/layout.component';
import { CourseViewComponent } from './components/course-view/course-view.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PaypalComponent,
    LayoutComponent,
    VideoViewComponent,
    CourseViewComponent
  ],
  imports: [
    NgxSpinnerModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    NgxPayPalModule,
    HttpClientModule,
    RouterModule.forRoot(RoutesHelper.routes),
    AuthModule,
    ChannelModule,
    SchoolModule,
    ProfileModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
