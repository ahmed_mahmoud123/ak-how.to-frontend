import { Routes } from '@angular/router';
import { LoginComponent } from '../modules/auth/login/login.component';
import { RegisterComponent } from '../modules/auth/register/register.component';
import { ForgotPasswordComponent } from '../modules/auth/forgot-password/forgot-password.component'
import { ResetPasswordComponent } from '../modules/auth/forgot-password/reset-password/reset-password.component';
import { ResetPasswordEmailSentComponent } from '../modules/auth/forgot-password/reset-password-email-sent/reset-password-email-sent.component';

import { HomeComponent } from '../components/home/home.component';
import { PaypalComponent } from '../components/paypal/paypal.component';
import { LayoutComponent } from '../components/layout/layout.component';
import { CreateChannelComponent } from '../modules/channel/create/create.component';
import { ListChannelsComponent } from '../modules/channel/list/list.component';
import { ChannelListVideosComponent } from '../modules/channel/list-videos/list-videos.component';


import { ViewChannelsComponent } from '../modules/channel/view-channel/view-channel.component';
import { ChannelPublishVideoComponent } from '../modules/channel/publish-video/publish-video.component';
import { UserProfileComponent } from '../modules/profile/user-profile/user-profile.component';


import { CreateSchoolComponent } from '../modules/school/create/create.component';
import { VideoViewComponent } from '../components/video-view/video-view.component';

import { ListSchoolsComponent } from '../modules/school/list/list.component';
import { ViewSchoolComponent } from '../modules/school/view-school/view-school.component';
import { CreateCourseComponent } from '../modules/school/create-course/create-course.component';

import { ListCoursesComponent } from '../modules/school/list-courses/list-courses.component';

import { BuildCourseComponent } from '../modules/school/build-course/build-course.component';
import { CourseViewComponent } from '../components/course-view/course-view.component';

export class RoutesHelper {

  static routes: Routes = [
    {
      path: 'dashboard',
      component: LayoutComponent,
      children: [
        {path: '',pathMatch: 'full', redirectTo: 'home'},
        {path: 'home', component: HomeComponent,data: { title: 'How.to | Home' }},
        {path: 'profile', component: UserProfileComponent,data: { title: 'How.to | User Profile' }},
        {path: 'video/:video_id', component: VideoViewComponent,data: { title: 'How.to | Video' }},
        {path: 'course/:course_id', component: CourseViewComponent,data: { title: 'How.to | Course' }},
        {path: 'channel/create', component: CreateChannelComponent,data: { title: 'How.to | Create Channel' }},
        {path: 'channel/list', component: ListChannelsComponent,data: { title: 'How.to | List Channels' }},
        {path: 'school/list', component: ListSchoolsComponent,data: { title: 'How.to | List Schools' }},
        {path: 'channel-videos/:channel_id', component: ChannelListVideosComponent,data: { title: 'How.to | Videos List' }},
        {path: 'build-course/:course_id', component: BuildCourseComponent,data: { title: 'How.to | Build Course' }},
        {path: 'channel/:channel_id', component: ViewChannelsComponent,
          children: [
            {path: '',pathMatch: 'full', redirectTo: 'publish'},
            {path: 'publish', component: ChannelPublishVideoComponent,data: { title: 'How.to | Publish Video' }},
            {path: 'videos', component: ChannelListVideosComponent,data: { title: 'How.to | Videos List' }},
            { path: '**', component: ChannelPublishVideoComponent }
          ]
        },
        {path: 'school/create', component: CreateSchoolComponent,data: { title: 'How.to | Create Channel' }},
        {path: 'school/:school_id', component: ViewSchoolComponent,
          children: [
            {path: '',pathMatch: 'full', redirectTo: 'create-course'},
            {path: 'create-course', component: CreateCourseComponent,data: { title: 'How.to | Create Course' }},
            {path: 'courses', component: ListCoursesComponent,data: { title: 'How.to | Courses List' }},
            { path: '**', component: ViewSchoolComponent }
          ]
        }

      ]
    },
    {
      path: "paypal",
      component: PaypalComponent,
      data: { title: 'How.to | Paypal' }
    },
    {
      path: "login",
      component: LoginComponent,
      data: { title: 'How.to | Login' }
    },
    {
      path: "signup",
      component: RegisterComponent,
      data: { title: 'How.to | Sign Up' }
    }
    ,
    {
      path: "forgotPassword",
      component: ForgotPasswordComponent,
      data: { title: 'How.to | Forgot Password' }
    },
    {
      path: "forgotPassword/resetPassword",
      component: ResetPasswordComponent,
      data: { title: 'How.to | Reset Password' }

    },
    {
      path: "forgotPassword/emailSent",
      component: ResetPasswordEmailSentComponent,
      data: { title: 'How.to | Email Sent !' }

    },
    {
      path: '',
      redirectTo: '/login',
      pathMatch: 'full'
    },
    { path: '**', component: LoginComponent }
  ]
}
