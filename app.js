var express = require('express');
var path = require("path");
var app = express();

/*
app.use(function (req, res, next) {
    //res.header('transfer-encoding', 'chunked');
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Headers', 'x-access-token,Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
});*/


app.use(express.static(path.join(__dirname, '/dist')));
app.get('/*', (req, resp) => {
    resp.sendFile(__dirname + "/dist/index.html");
});

app.listen(4000);